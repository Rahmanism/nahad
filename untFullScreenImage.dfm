object frmFullScreenImage: TfrmFullScreenImage
  Left = 199
  Top = 110
  BiDiMode = bdRightToLeft
  BorderStyle = bsNone
  ClientHeight = 506
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ParentBiDiMode = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object imgFullScreenImage: TImage
    Left = 0
    Top = 0
    Width = 775
    Height = 506
    Cursor = 1
    Align = alClient
    Center = True
    Proportional = True
    OnClick = imgFullScreenImageClick
  end
end
