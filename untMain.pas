unit untMain;

interface              

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, MPlayer, Buttons, ComCtrls, MyVolControl, jpeg,
  ActnList, ShellAPI, Menus, OleCtrls, ImgList, untText, untImage, untVideo,
  untAudio, untClip, untPlayer, General;

const
  WM_ICONTRAY = WM_USER + 1;

type
  TFunctionType = (ftAudio, ftVideo, ftImage, ftText, ftClip);

  TfrmMain = class(TForm)
    imgMain: TImage;
    tmrBlend: TTimer;
    mplBack: TMediaPlayer;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    ImageList1: TImageList;
    fraText1: TfraText;
    fraAudio1: TfraAudio;
    fraVideo1: TfraVideo;
    fraClip1: TfraClip;
    imgAudio: TImage;
    lblAudio: TLabel;
    imgImage: TImage;
    lblImage: TLabel;
    imgClip: TImage;
    lblClip: TLabel;
    imgVideo: TImage;
    lblVideo: TLabel;
    imgText: TImage;
    lblText: TLabel;
    imgExit: TImage;
    lblExit: TLabel;
    imgMin: TImage;
    lblMin: TLabel;
    imgSysTray: TImage;
    lblSysTray: TLabel;
    imgMusic: TImage;
    lblMusic: TLabel;
    fraImage1: TfraImage;
    ActionList1: TActionList;
    actPlayPause: TAction;
    actStop: TAction;
    actRewind: TAction;
    actForward: TAction;
    actPrevious: TAction;
    actNext: TAction;
    actVolHigh: TAction;
    actVolLow: TAction;
    actMute: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblMinMouseEnter(Sender: TObject);
    procedure lblMinMouseLeave(Sender: TObject);
    procedure lblMinClick(Sender: TObject);
    procedure lblExitMouseEnter(Sender: TObject);
    procedure lblExitMouseLeave(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tmrBlendTimer(Sender: TObject);
    procedure mplBackNotify(Sender: TObject);
    procedure lblMusicMouseEnter(Sender: TObject);
    procedure lblMusicMouseLeave(Sender: TObject);
    procedure lblMusicClick(Sender: TObject);
    procedure lblSysTrayClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure lblSysTrayMouseEnter(Sender: TObject);
    procedure lblSysTrayMouseLeave(Sender: TObject);
    procedure lblAudioMouseEnter(Sender: TObject);
    procedure lblAudioMouseLeave(Sender: TObject);
    procedure lblVideoMouseEnter(Sender: TObject);
    procedure lblVideoMouseLeave(Sender: TObject);
    procedure lblVideoClick(Sender: TObject);
    procedure lblAudioClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure fraPlayer1actPlayPauseExecute(Sender: TObject);
    procedure fraPlayer1actMuteExecute(Sender: TObject);
    procedure lblClipClick(Sender: TObject);
    procedure lblTextClick(Sender: TObject);
    procedure lblImageMouseEnter(Sender: TObject);
    procedure lblImageMouseLeave(Sender: TObject);
    procedure lblTextMouseEnter(Sender: TObject);
    procedure lblTextMouseLeave(Sender: TObject);
    procedure lblClipMouseEnter(Sender: TObject);
    procedure lblClipMouseLeave(Sender: TObject);
    procedure lblImageClick(Sender: TObject);
    procedure actPlayPauseExecute(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure actRewindExecute(Sender: TObject);
    procedure actForwardExecute(Sender: TObject);
    procedure actPreviousExecute(Sender: TObject);
    procedure actNextExecute(Sender: TObject);
    procedure actVolHighExecute(Sender: TObject);
    procedure actVolLowExecute(Sender: TObject);
    procedure actMuteExecute(Sender: TObject);
  private
    TrayIconData: TNotifyIconData;
    procedure InitialTrayIcon;
    procedure TrayMessage(var Msg: TMessage); message WM_ICONTRAY;
    procedure SwitchFunctionType(FT: TFunctionType);
    procedure DeactivateActionList;
    procedure ActivateActionList;
    procedure LoadImages;
    procedure LoadfraAudioImages;
    procedure LoadfraClipImages;
    procedure LoadfraImageImages;
    procedure LoadfraTextImages;
    procedure LoadfraVideoImages;
    procedure LoadfraPlayerAudioImages;
//    function GetWinDir: TFileName;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MaxNo: Byte;
  frmMain: TfrmMain;
  mplBackPlay: Boolean;
  NowPlay: Byte;  // 0=none
  Mute: Boolean;
  Rep, Loop: Boolean;
  FunctionType: TFunctionType;
  Vol: Smallint;
  fraP: TfraPlayer;

implementation

uses Math, untSplash, untBackground, untFullScreen, Types,
  untBesm;

{$R *.dfm}

procedure TfrmMain.InitialTrayIcon;
begin
  PopUpMenu1.OwnerDraw:=True;

  with TrayIconData do
  begin
    cbSize := SizeOf(TrayIconData);
    Wnd := Handle;
    uID := 0;
    uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
    uCallbackMessage := WM_ICONTRAY;
    hIcon := Application.Icon.Handle;
    StrPCopy(szTip, Application.Title);
  end;

  Shell_NotifyIcon(NIM_ADD, @TrayIconData);
end;

procedure TfrmMain.TrayMessage(var Msg: TMessage);
var
  p : TPoint;
begin
{  case Msg.lParam of
    WM_LBUTTONDOWN:
    begin
      ShowMessage('This icon responds to RIGHT BUTTON click!');
    end;
    WM_RBUTTONDOWN:
    begin
       SetForegroundWindow(Handle);
       GetCursorPos(p);
       PopUpMenu1.Popup(p.x, p.y);
       PostMessage(Handle, WM_NULL, 0, 0);
    end;
  end;}
  if (Msg.LParam = WM_LBUTTONDOWN) or (Msg.LParam = WM_RBUTTONDOWN) then
  begin
    SetForegroundWindow(Handle);
    GetCursorPos(p);
    PopUpMenu1.Popup(p.x, p.y);
    PostMessage(Handle, WM_NULL, 0, 0);
  end;
end;



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////////                                      ///////////////////////
//////////////////     L O A D I N G   I M A G E S      ///////////////////////
//////////////////                                      ///////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
procedure TfrmMain.LoadfraPlayerAudioImages;
begin
  ImageLoad(fraAudio1.fraPlayer1.imgPlayer, 'fraPlayer_imgPlayer.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPre, 'fraPlayer_imgPre.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPreD, 'fraPlayer_imgPreD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgRewind, 'fraPlayer_imgRewind.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgRwD, 'fraPlayer_imgRwD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPlay, 'fraPlayer_imgPlay.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPlayD, 'fraPlayer_imgPlayD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPause, 'fraPlayer_imgPause.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPauseD, 'fraPlayer_imgPauseD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgPauseO, 'fraPlayer_imgPauseO.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgStop, 'fraPlayer_imgStop.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgStopD, 'fraPlayer_imgStopD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgForward, 'fraPlayer_imgForward.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgFwD, 'fraPlayer_imgFwD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgNext, 'fraPlayer_imgNext.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgNextD, 'fraPlayer_imgNextD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgRepeat, 'fraPlayer_imgRepeat.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgRepD, 'fraPlayer_imgRepD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgLoop, 'fraPlayer_imgLoop.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgLoopD, 'fraPlayer_imgLoopD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgMute, 'fraPlayer_imgMute.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgMuteD, 'fraPlayer_imgMuteD.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgSound, 'fraPlayer_imgSound.jpg');
  ImageLoad(fraAudio1.fraPlayer1.imgTrackMove, 'fraPlayer_imgTrackMove.jpg');
end;

procedure TfrmMain.LoadfraAudioImages;
begin
  ImageLoad(fraAudio1.Image1, 'fraAudio_Image1.jpg');
  ImageLoad(fraAudio1.imgDisplayPanel, 'fraAudio_imgDisplayPanel.jpg');
  ImageLoad(fraAudio1.fraBtns1.Image1, 'fraBtns_Image1.jpg');
  LoadfraPlayerAudioImages;
end;

procedure TfrmMain.LoadfraClipImages;
begin
  ImageLoad(fraClip1.Image1, 'fraClip_Image1.jpg');
  ImageLoad(fraClip1.imgDisplayPanel, 'fraClip_imgDisplayPanel.jpg');
  ImageLoad(fraClip1.fraPlayer1.imgPlayer, 'fraPlayer_imgPlayer.jpg');
end;

procedure TfrmMain.LoadfraVideoImages;
begin
  ImageLoad(fraVideo1.Image1, 'fraVideo_Image1.jpg');
  ImageLoad(fraVideo1.imgDisplayPanel, 'fraVideo_imgDisplayPanel.jpg');
  ImageLoad(fraVideo1.fraPlayer1.imgPlayer, 'fraPlayer_imgPlayer.jpg');
  ImageLoad(fraVideo1.fraBtns1.Image1, 'fraBtns_Image1.jpg');
end;

procedure TfrmMain.LoadfraImageImages;
begin
  ImageLoad(fraImage1.imgImageBack, 'fraImage_imgImageBack.jpg');
  ImageLoad(fraImage1.imgNextImage, 'fraImage_imgNextImage.jpg');
  ImageLoad(fraImage1.imgPreImage, 'fraImage_imgPreImage.jpg');
end;

procedure TfrmMain.LoadfraTextImages;
begin
  ImageLoad(fraText1.Image1, 'fraText_Image1.jpg');
end;

procedure TfrmMain.LoadImages;
begin
  ImageLoad(imgAudio, 'imgAudio.jpg');
  ImageLoad(imgClip, 'imgClip.jpg');
  ImageLoad(imgExit, 'imgExit.jpg');
  ImageLoad(imgImage, 'imgImage.jpg');
  ImageLoad(imgMain, 'imgMain.jpg');
  ImageLoad(imgMin, 'imgMin.jpg');
  ImageLoad(imgMusic, 'imgMusic.jpg');
  ImageLoad(imgMin, 'imgMin.jpg');
  ImageLoad(imgSysTray, 'imgSysTray.jpg');
  ImageLoad(imgText, 'imgText.jpg');
  ImageLoad(imgVideo, 'imgVideo.jpg');
end;
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//////////////////                                      ///////////////////////
//////////////////     L O A D I N G   I M A G E S      ///////////////////////
//////////////////                                      ///////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Screen.Cursors[1] := 31;

  frmBackground := TfrmBackground.Create(Self);
  frmBackground.Show;
  frmBackground.Enabled := False;

  frmBesm := TfrmBesm.Create(Self);
  frmBesm.ShowModal;

  frmSplash := TfrmSplash.Create(Self);
  frmSplash.ShowModal;

  Caption := Application.Title;

  LoadImages;

  Rep := False;
  Loop := False;
  NowPlay := 0; // No file is playing.
  ReadOrCreateIni('Main');
  SwitchFunctionType(ftVideo);
  Vol := MPGetVolume(fraVideo1.fraPlayer1.mplPlayer);
  Mute := False;
  fraVideo1.fraPlayer1.imgMute.Visible := False;
  fraVideo1.fraPlayer1.SoundHeightShow;
  fraP := fraVideo1.fraPlayer1;
  try
    mplBack.FileName := DataPath + 'b.mp3';
    mplBack.Open;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
  except;end;
end;

(*function TForm1.GetWinDir: TFileName;
var
  SysFolder: String;
begin
  SetLength(SysFolder, 255);
  SetLength(SysFolder, GetWindowsDirectory(
    PChar(SysFolder), Length(SysFolder)));
  Result := SysFolder;
end;*)

procedure TfrmMain.FormShow(Sender: TObject);
begin
  tmrBlend.Enabled := True;
  fraVideo1.fraPlayer1.mplPlayer.Notify := True;
end;

procedure TfrmMain.lblMinMouseEnter(Sender: TObject);
begin
  imgMin.Visible := True;
end;

procedure TfrmMain.lblMinMouseLeave(Sender: TObject);
begin
  imgMin.Visible := False;
end;

procedure TfrmMain.lblMinClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TfrmMain.lblExitMouseEnter(Sender: TObject);
begin
  imgExit.Visible := True;
end;

procedure TfrmMain.lblExitMouseLeave(Sender: TObject);
begin
  imgExit.Visible := False;
end;

procedure TfrmMain.lblExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  try
    mplBack.Close;
    //MediaPlayer1.Close;
    //Timer1.Enabled := False;
  except;end;
  if Tag = 0 then
  begin
    CanClose := False;
    frmMain.AlphaBlend := True;
    Tag := 1;
    tmrBlend.Enabled := True;
  end;
end;

procedure TfrmMain.tmrBlendTimer(Sender: TObject);
var
  ab: Integer;
begin
  if Tag = 0 then
  begin
    ab := frmMain.AlphaBlendValue + 50;
    if ab > 255 then
    begin
      ab := 255;
      frmMain.AlphaBlendValue := ab;
      frmMain.AlphaBlend := False;
      tmrBlend.Enabled := False;
    end
    else
      frmMain.AlphaBlendValue := ab;
  end
  else if Tag = 1 then
  begin
    ab := frmMain.AlphaBlendValue - 50;
    if ab < 0 then
    begin
      ab := 0;
      frmMain.AlphaBlendValue := ab;
      tmrBlend.Enabled := False;
      //swfMovie := DataPath + 'Sys03.dll';
      //frmSplash := TfrmSplash.Create(Self);
      //frmSplash.Timer1.Interval := 1000;//47000;
      //frmSplash.ShowModal;
      Close;
    end
    else
      frmMain.AlphaBlendValue := ab;
  end
end;

procedure TfrmMain.mplBackNotify(Sender: TObject);
begin
  try
    if (mplBack.Mode = mpStopped) and (mplBackPlay = True) then
    begin
      mplBack.Play;
      mplBackPlay := True;
    end
    else if mplBack.Position >= mplBack.Length then
    begin
      mplBack.Rewind;
      mplBack.Play;
    end;
  except;end;
  mplBack.Notify := True;
end;

procedure TfrmMain.lblMusicMouseEnter(Sender: TObject);
begin
  imgMusic.Visible := True;
end;

procedure TfrmMain.lblMusicMouseLeave(Sender: TObject);
begin
  imgMusic.Visible := False;
end;

procedure TfrmMain.lblMusicClick(Sender: TObject);
begin
//  Imaging(0);
  try
    case FunctionType of
      ftVideo:
        begin
          if fraVideo1.fraPlayer1.mplPlayer.Mode = mpPlaying then
            fraVideo1.fraPlayer1.mplPlayer.Stop;
          fraVideo1.fraPlayer1.mplPlayer.Notify := False;
        end;
      ftAudio:
        begin
          if fraAudio1.fraPlayer1.mplPlayer.Mode = mpPlaying then
            fraAudio1.fraPlayer1.mplPlayer.Stop;
          fraAudio1.fraPlayer1.mplPlayer.Notify := False;
        end;
      ftClip:
        begin
          if fraClip1.fraPlayer1.mplPlayer.Mode = mpPlaying then
            fraClip1.fraPlayer1.mplPlayer.Stop;
          fraClip1.fraPlayer1.mplPlayer.Notify := False;
        end;
    end;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
    case FunctionType of
      ftVideo:
        begin
          with fraVideo1.fraPlayer1 do
          begin
            mplPlayer.Notify := True;
            imgPause.Visible := False;
            imgPauseD.Visible := False;
            imgPauseO.Visible := False;
          end;
        end;
      ftAudio:
        begin
          with fraAudio1.fraPlayer1 do
          begin
            mplPlayer.Notify := True;
            imgPause.Visible := False;
            imgPauseD.Visible := False;
            imgPauseO.Visible := False;
          end;
        end;
      ftClip:
        begin
          with fraClip1.fraPlayer1 do
          begin
            mplPlayer.Notify := True;
            imgPause.Visible := False;
            imgPauseD.Visible := False;
            imgPauseO.Visible := False;
          end;
        end;
    end;
  except;
  end;
end;

procedure TfrmMain.lblSysTrayClick(Sender: TObject);
begin
  InitialTrayIcon;
  try
    case FunctionType of
      ftVideo:
        begin
          with fraVideo1.fraPlayer1 do
            if mplPlayer.Mode = mpPlaying then
              mplPlayer.Stop;
        end;
      ftAudio:
        begin
          with fraAudio1.fraPlayer1 do
            if mplPlayer.Mode = mpPlaying then
              mplPlayer.Stop;
        end;
      ftClip:
        begin
          with fraClip1.fraPlayer1 do
            if mplPlayer.Mode = mpPlaying then
              mplPlayer.Stop;
        end;
    end;
    if mplBack.Mode = mpPlaying then
    begin
      mplBack.Stop;
      mplBackPlay := False;
    end;
  except;end;
  Hide;
  frmBackground.Hide;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMain.N1Click(Sender: TObject);
begin
  frmBackground.Show;
  frmMain.Show;
  if mplBackPlay then
    mplBack.Play;
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMain.N2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.N8Click(Sender: TObject);
begin
  if (mplBack.Mode = mpPlaying) then
  begin
    mplBack.Stop;
    mplBackPlay := False;
  end;
end;

procedure TfrmMain.lblSysTrayMouseEnter(Sender: TObject);
begin
  imgSysTray.Visible := True;
end;

procedure TfrmMain.lblSysTrayMouseLeave(Sender: TObject);
begin
  imgSysTray.Visible := False;
end;

procedure TfrmMain.lblAudioMouseEnter(Sender: TObject);
begin
  imgAudio.Visible := True;
end;

procedure TfrmMain.lblAudioMouseLeave(Sender: TObject);
begin
  if not (FunctionType = ftAudio) then
    imgAudio.Visible := False;
end;

procedure TfrmMain.lblVideoMouseEnter(Sender: TObject);
begin
  imgVideo.Visible := True;
end;

procedure TfrmMain.lblVideoMouseLeave(Sender: TObject);
begin
  if not (FunctionType = ftVideo) then
    imgVideo.Visible := False;
end;

procedure TfrmMain.SwitchFunctionType(FT: TFunctionType);
begin
  FunctionType := FT;
  fraVideo1.Visible := False;
  fraVideo1.Enabled := False;
  imgVideo.Visible := False;
  fraClip1.Visible := False;
  fraClip1.Enabled := False;
  imgClip.Visible := False;
  fraAudio1.Visible := False;
  fraAudio1.Enabled := False;
  imgAudio.Visible := False;
  fraImage1.Visible := False;
  fraImage1.Enabled := False;
  imgImage.Visible := False;
  fraText1.Visible := False;
  fraText1.Enabled := False;
  imgText.Visible := False;
  try
    fraAudio1.fraPlayer1.mplPlayer.Close;
    fraVideo1.fraPlayer1.mplPlayer.Close;
    fraClip1.fraPlayer1.mplPlayer.Close;
  except; end;
  case FT of
    ftAudio:
      begin
        ReadOrCreateIni('Audio');
        LoadfraAudioImages;
        fraAudio1.Visible := True;
        fraAudio1.Enabled := True;
        MaxNo := 5;
        imgAudio.Visible := True;
        fraP := fraAudio1.fraPlayer1;
        ActivateActionList;
      end;
    ftVideo:
      begin
        ReadOrCreateIni('Video');
        LoadfraVideoImages;
        fraVideo1.Visible := True;
        fraVideo1.Enabled := True;
        fraVideo1.fraPlayer1.mplPlayer.Display := fraVideo1.pnlScreen;
        MaxNo := 5;
        imgVideo.Visible := True;
        fraP := fraVideo1.fraPlayer1;
        ActivateActionList;
      end;
    ftImage:
      begin
        ReadOrCreateIni('Image');
        LoadfraImageImages;
        fraImage1.Visible := True;
        fraImage1.Enabled := True;
        fraImage1.FirstLoad;
        imgImage.Visible := True;
        lblMusicClick(Self);
        fraP := nil;
        DeactivateActionList;
      end;
    ftClip:
      begin
        ReadOrCreateIni('Clip');
        LoadfraClipImages;
        fraClip1.Visible := True;
        fraClip1.Enabled := True;
        fraClip1.fraPlayer1.mplPlayer.Display := fraClip1.pnlScreen;
        fraClip1.fraPlayer1.PlayFile(1);
        MaxNo := 1;
        imgClip.Visible := True;
        fraP := fraClip1.fraPlayer1;
        ActivateActionList;
      end;
    ftText:
      begin
        ReadOrCreateIni('Text');
        LoadfraTextImages;
        fraText1.Visible := True;
        fraText1.Enabled := True;
        imgText.Visible := True;
        fraText1.WebBrowser1.Navigate(MyDir(TEXT_FOLDER + 'n.htm'));
        fraP := nil;
        DeactivateActionList;
      end;
  end;
  ReadOrCreateIni('Btns');
end;

procedure TfrmMain.DeactivateActionList;
var
  i: Byte;
begin
  for i := 0 to ActionList1.ActionCount - 1 do
    (ActionList1.Actions[i] as TAction).Enabled := False;
end;

procedure TfrmMain.ActivateActionList;
var
  i: Byte;
begin
  for i := 0 to ActionList1.ActionCount - 1 do
    (ActionList1.Actions[i] as TAction).Enabled := True;
end;

procedure TfrmMain.lblVideoClick(Sender: TObject);
begin
  SwitchFunctionType(ftVideo);
end;

procedure TfrmMain.lblAudioClick(Sender: TObject);
begin
  SwitchFunctionType(ftAudio);
end;

procedure TfrmMain.N5Click(Sender: TObject);
begin
  case FunctionType of
    ftVideo: fraVideo1.fraPlayer1.lblPlayPauseClick(Sender);
    ftAudio: fraAudio1.fraPlayer1.lblPlayPauseClick(Sender);
    ftClip : fraClip1.fraPlayer1.lblPlayPauseClick(Sender);
  end;
end;

procedure TfrmMain.N6Click(Sender: TObject);
begin
  case FunctionType of
    ftVideo: fraVideo1.fraPlayer1.lblStopClick(Sender);
    ftAudio: fraAudio1.fraPlayer1.lblStopClick(Sender);
    ftClip : fraClip1.fraPlayer1.lblStopClick(Sender);
  end;
end;

procedure TfrmMain.fraPlayer1actPlayPauseExecute(Sender: TObject);
begin
  inherited;
  ;
end;

procedure TfrmMain.fraPlayer1actMuteExecute(Sender: TObject);
begin
  inherited;
  ;
end;

procedure TfrmMain.lblClipClick(Sender: TObject);
begin
  SwitchFunctionType(ftClip);
end;

procedure TfrmMain.lblTextClick(Sender: TObject);
begin
  SwitchFunctionType(ftText);
end;

procedure TfrmMain.lblImageMouseEnter(Sender: TObject);
begin
  imgImage.Visible := True;
end;

procedure TfrmMain.lblImageMouseLeave(Sender: TObject);
begin
  if not (FunctionType = ftImage) then
    imgImage.Visible := False;
end;

procedure TfrmMain.lblTextMouseEnter(Sender: TObject);
begin
  imgText.Visible := True;
end;

procedure TfrmMain.lblTextMouseLeave(Sender: TObject);
begin
  if not (FunctionType = ftText) then
    imgText.Visible := False;
end;

procedure TfrmMain.lblClipMouseEnter(Sender: TObject);
begin
  imgClip.Visible := True;
end;

procedure TfrmMain.lblClipMouseLeave(Sender: TObject);
begin
  if not (FunctionType = ftClip) then
    imgClip.Visible := False;
end;

procedure TfrmMain.lblImageClick(Sender: TObject);
begin
  SwitchFunctionType(ftImage);
end;

procedure TfrmMain.actPlayPauseExecute(Sender: TObject);
begin
  fraP.actPlayPause.OnExecute(Sender);
end;

procedure TfrmMain.actStopExecute(Sender: TObject);
begin
  fraP.actStop.OnExecute(Sender);
end;

procedure TfrmMain.actRewindExecute(Sender: TObject);
begin
  fraP.actRewind.OnExecute(Sender);
end;

procedure TfrmMain.actForwardExecute(Sender: TObject);
begin
  fraP.actForward.OnExecute(Sender);
end;

procedure TfrmMain.actPreviousExecute(Sender: TObject);
begin
  fraP.actPrevious.OnExecute(Sender);
end;

procedure TfrmMain.actNextExecute(Sender: TObject);
begin
  fraP.actNext.OnExecute(Sender);
end;

procedure TfrmMain.actVolHighExecute(Sender: TObject);
begin
  fraP.actVolHigh.OnExecute(Sender);
end;

procedure TfrmMain.actVolLowExecute(Sender: TObject);
begin
  fraP.actVolLow.OnExecute(Sender);
end;

procedure TfrmMain.actMuteExecute(Sender: TObject);
begin
  fraP.actMute.OnExecute(Sender);
end;

end.
