program Nahad;

uses
  Forms,
  untMain in 'untMain.pas' {frmMain},
  untBackground in 'untBackground.pas' {frmBackground},
  untFullScreen in 'untFullScreen.pas' {frmFullScreen},
  untBesm in 'untBesm.pas' {frmBesm},
  untSplash in 'untSplash.pas' {frmSplash},
  untText in 'untText.pas' {fraText: TFrame},
  untImage in 'untImage.pas' {fraImage: TFrame},
  untVideo in 'untVideo.pas' {fraVideo: TFrame},
  untAudio in 'untAudio.pas' {fraAudio: TFrame},
  untClip in 'untClip.pas' {fraClip: TFrame},
  untPlayer in 'untPlayer.pas' {fraPlayer: TFrame},
  untBtns in 'untBtns.pas' {fraBtns: TFrame},
  untFullScreenImage in 'untFullScreenImage.pas' {frmFullScreenImage},
  General in 'General.pas';

{$R *.res}

begin
  Application.Initialize;
  ReadOrCreateIni('Application');
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
