unit untFullScreenImage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TfrmFullScreenImage = class(TForm)
    imgFullScreenImage: TImage;
    procedure imgFullScreenImageClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFullScreenImage: TfrmFullScreenImage;

implementation

uses
  untAudio;

{$R *.dfm}

procedure TfrmFullScreenImage.imgFullScreenImageClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFullScreenImage.FormKeyPress(Sender: TObject; var Key: Char);
begin
  Close;
end;

procedure TfrmFullScreenImage.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FullScreen := False;
  Action := caFree;
end;

end.
