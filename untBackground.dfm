object frmBackground: TfrmBackground
  Left = 32
  Top = 92
  AlphaBlend = True
  AlphaBlendValue = 0
  BorderStyle = bsNone
  ClientHeight = 768
  ClientWidth = 1024
  Color = clWhite
  TransparentColorValue = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object imgBackground: TImage
    Left = 0
    Top = 0
    Width = 1024
    Height = 768
    Align = alClient
    Center = True
    OnClick = imgBackgroundClick
  end
  object tmrBlend: TTimer
    Interval = 70
    OnTimer = tmrBlendTimer
    Left = 128
    Top = 8
  end
end
