unit untBackground;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, jpeg, General;

type
  TfrmBackground = class(TForm)
    imgBackground: TImage;
    tmrBlend: TTimer;
    procedure imgBackgroundClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmrBlendTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBackground: TfrmBackground;

implementation

{$R *.dfm}

procedure TfrmBackground.imgBackgroundClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmBackground.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key in [#32, #27, #13]) then
    Close;
end;

procedure TfrmBackground.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBackground.tmrBlendTimer(Sender: TObject);
var
  ab: Integer;
begin
  if Tag = 0 then
  begin
    ab := AlphaBlendValue + ALPHABLEND_DIFF;
    if ab > 255 then
    begin
      ab := 255;
      AlphaBlendValue := ab;
      AlphaBlend := False;
      tmrBlend.Enabled := False;
//      Self.Enabled := False;
//      Timer1.Enabled := True;
    end
    else
      AlphaBlendValue := ab;
  end
  else if Tag = 1 then
  begin
    ab := AlphaBlendValue - ALPHABLEND_DIFF;
    if ab < 0 then
    begin
      ab := 0;
      AlphaBlendValue := ab;
      tmrBlend.Enabled := False;
      Close;
    end
    else
      AlphaBlendValue := ab;
  end
end;

procedure TfrmBackground.FormCreate(Sender: TObject);
begin
  Tag := 0;
end;

procedure TfrmBackground.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if Tag = 0 then
  begin
    Tag := 1;
    CanClose := False;
    AlphaBlend := True;
    tmrBlend.Enabled := True;
  end
  else
    CanClose := True;
end;

end.
