unit untFullScreen;

interface

uses
  untMain, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, ActnList, jpeg, MPlayer;

type
  TfrmFullScreen = class(TForm)
    Panel1: TPanel;
    ActionList1: TActionList;
    actPlayPauseF: TAction;
    actStopF: TAction;
    actRewindF: TAction;
    actForwardF: TAction;
    actPreviousF: TAction;
    actNextF: TAction;
    actVolHighF: TAction;
    actVolLowF: TAction;
    actMuteF: TAction;
    imgFullScreen: TImage;
    imgExit: TImage;
    lblExit: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel1DblClick(Sender: TObject);
    procedure actPlayPauseFExecute(Sender: TObject);
    procedure actStopFExecute(Sender: TObject);
    procedure actRewindFExecute(Sender: TObject);
    procedure actForwardFExecute(Sender: TObject);
    procedure actPreviousFExecute(Sender: TObject);
    procedure actNextFExecute(Sender: TObject);
    procedure actVolHighFExecute(Sender: TObject);
    procedure actVolLowFExecute(Sender: TObject);
    procedure actMuteFExecute(Sender: TObject);
    procedure lblExitMouseEnter(Sender: TObject);
    procedure lblExitMouseLeave(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFullScreen: TfrmFullScreen;

implementation

uses untVideo;

{$R *.dfm}

procedure TfrmFullScreen.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  t: TRect;
  fra1: TComponent;
begin
  fra1 := nil;
  if FunctionType = ftVideo then
  begin
    fra1 := frmMain.fraVideo1.fraPlayer1.mplPlayer;
    (fra1 as TMediaPlayer).Display := frmMain.fraVideo1.pnlScreen;
  end
  else if FunctionType = ftClip then
  begin
    fra1 := frmMain.fraClip1.fraPlayer1.mplPlayer;
    (fra1 as TMediaPlayer).Display := frmMain.fraClip1.pnlScreen;
  end;
  with (fra1 as TMediaPlayer) do
  begin
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := Display.Width;
      Bottom := Display.Height;
    end;
    DisplayRect := t;
  end;

  Action := caFree;
end;

procedure TfrmFullScreen.Panel1DblClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFullScreen.actPlayPauseFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actPlayPause.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actPlayPause.OnExecute(Sender)
end;

procedure TfrmFullScreen.actStopFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actStop.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actStop.OnExecute(Sender)
end;

procedure TfrmFullScreen.actRewindFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actRewind.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actRewind.OnExecute(Sender)
end;

procedure TfrmFullScreen.actForwardFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actForward.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actForward.OnExecute(Sender)
end;

procedure TfrmFullScreen.actPreviousFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actPrevious.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actPrevious.OnExecute(Sender)
end;

procedure TfrmFullScreen.actNextFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actNext.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actNext.OnExecute(Sender)
end;

procedure TfrmFullScreen.actVolHighFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actVolHigh.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actVolHigh.OnExecute(Sender)
end;

procedure TfrmFullScreen.actVolLowFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actVolLow.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actVolLow.OnExecute(Sender)
end;

procedure TfrmFullScreen.actMuteFExecute(Sender: TObject);
begin
  if FunctionType = ftVideo then
    frmMain.fraVideo1.fraPlayer1.actMute.OnExecute(Sender)
  else if FunctionType = ftClip then
    frmMain.fraClip1.fraPlayer1.actMute.OnExecute(Sender)
end;

procedure TfrmFullScreen.lblExitMouseEnter(Sender: TObject);
begin
  imgExit.Visible := True;
end;

procedure TfrmFullScreen.lblExitMouseLeave(Sender: TObject);
begin
  imgExit.Visible := False;
end;

procedure TfrmFullScreen.lblExitClick(Sender: TObject);
begin
  Close;
end;

end.
